
package milestone.coursework.Servlets;

import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class BaseServlet extends HttpServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(BaseServlet.class);

    public static final  String PLAIN_TEXT_UTF_8 = "text/plain; charset=UTF-8";
    public static final Charset CHARSET_UTF8 = Charset.forName("UTF-8");
    public static final String HTML_UTF_8 = "text/html; charset=UTF-8";
    static final String DEFAULT_PAGE = "/";

    static final Set<String> PUBLIC_PAGES = new HashSet<>(Arrays.asList("/milestone/login", "/milestone/registration"));

    public final MustacheRender mustache;

    protected BaseServlet() { mustache = new MustacheRender();
    }

    protected void issue(String mimeType, int returnCode, byte[] output, HttpServletResponse response) throws IOException {
        response.setContentType(mimeType);
        response.setStatus(returnCode);
        response.getOutputStream().write(output);
    }

    protected void cache(HttpServletResponse response, int seconds) {
        if (seconds > 0) {
            response.setHeader("Pragma", "Public");
            response.setHeader("Cache-Control", "public, no-transform, max-age=" + seconds);
        }
    }

    protected  void showView(HttpServletResponse response, String templateName, Object model) throws IOException {

            String html = mustache.render(templateName, model);
            issue(HTML_UTF_8, HttpServletResponse.SC_OK, html.getBytes(CHARSET_UTF8), response);

    }


    boolean authOK(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String uri = request.getRequestURI();
        HttpSession session = request.getSession(false);

        if (!PUBLIC_PAGES.contains(uri) && session == null) {
            response.sendRedirect(DEFAULT_PAGE);
            return false;
        }
        return true;
    }
}
