package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.Milestone;
import milestone.coursework.BaseClasses.MilestoneBoard;
import milestone.coursework.BaseClasses.MilestoneProjects;
import milestone.coursework.BaseClasses.User;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class MilestoneBoardServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);
    private  H2Database h2Database ;



    public MilestoneBoardServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }

    public MilestoneBoardServlet() {
        super();
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {

            String url = request.getRequestURL().toString();
            String[] segments = url.split("/");
            String idStr = segments[segments.length-1];

            if(!idStr.equals("board")){

                int id = Integer.parseInt(idStr);
                MilestoneBoard milestoneBoard = h2Database.findMilestoneBoard(id);

                if(milestoneBoard.isShareable()) {

                    HttpSession session = request.getSession(false);
                    session.setAttribute("MilestoneBoardID", id);
                    h2Database.shareMilestoneBoard((int) session.getAttribute("MilestoneProjectsID"), (int) session.getAttribute("MilestoneBoardID"));
                    milestoneBoard.sortMilestones();
                    showView(response, "src/main/resources/webapp/MilestoneBoardView.html", milestoneBoard);
                }
            }
            else {

                MilestoneBoard milestoneBoard = h2Database.findMilestoneBoard((int) request.getSession().getAttribute("MilestoneBoardID"));
                milestoneBoard.sortMilestones();
                showView(response, "src/main/resources/webapp/MilestoneBoardView.html", milestoneBoard);
            }

        }

    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String[] selectedMilestones = request.getParameterValues("MilestoneBoard");

        String button1 = request.getParameter("btnGoTo");
        String button2 = request.getParameter("btnDelete");


        if(button1!=null){
            //Go to the specific Milestone which id is the only value in selectedMilestoneBoards
            HttpSession session = request.getSession(false);
            session.setAttribute("MilestoneID", Integer.parseInt(selectedMilestones[0]));
            String sessionid = session.getId();
            response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");
            response.sendRedirect(response.encodeRedirectURL("/milestone/board-milestone"));

        }
        else if(button2!=null){

            if(selectedMilestones.length > 0) {
                for (String s : selectedMilestones) {
                    h2Database.deleteMilestone((int)request.getSession().getAttribute("MilestoneBoardID"), Integer.parseInt(s));

                }
            }

            response.sendRedirect(response.encodeRedirectURL("/milestone/board/"));
        }
        else{

            HttpSession session = request.getSession(false);
            String sessionid = session.getId();
            response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");
            response.sendRedirect(response.encodeRedirectURL("/milestone/menu/AddMilestone"));
        }

    }

}

