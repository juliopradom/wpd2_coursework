package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MenuServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(MenuServlet.class);

    private final H2Database h2Database;

    public MenuServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {


            MilestoneProjects milestoneProjects = h2Database.findMilestoneProjects((String)request.getSession().getAttribute("user"));
            HttpSession session = request.getSession(false);
            session.setAttribute("MilestoneProjectsID", milestoneProjects.getID());




            showView(response, "src/main/resources/webapp/ProjectsView.html", milestoneProjects);


        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

       String[] selectedMilestoneProjects = request.getParameterValues("MilestoneProject");

       String button1 = request.getParameter("btnGoTo");
       String button2 = request.getParameter("btnDelete");


       if(button1!=null){
           //Go to the specific MilestoneBoard which id is the only value in selectedMilestoneProjects
           HttpSession session = request.getSession(false);
           session.setAttribute("MilestoneBoardID", Integer.parseInt(selectedMilestoneProjects[0]));

           String sessionid = session.getId();
           response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");
           response.sendRedirect(response.encodeRedirectURL("/milestone/board/"));

       }
       else if(button2!=null){

           if(selectedMilestoneProjects.length > 0) {
               for (String s : selectedMilestoneProjects) {
                   h2Database.deleteMilestoneBoard((int)request.getSession().getAttribute("MilestoneProjectsID"), Integer.parseInt(s));

               }
           }


           response.sendRedirect(response.encodeRedirectURL("/milestone/menu"));
       }
       else{

           HttpSession session = request.getSession(false);
           String sessionid = session.getId();
           response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");
           response.sendRedirect(response.encodeRedirectURL("/milestone/menu/AddProject"));
       }

    }


}
