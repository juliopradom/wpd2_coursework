package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AboutAuthorsServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);


    public AboutAuthorsServlet() {
        super();
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {


            showView(response, "src/main/resources/webapp/AboutAuthorsView.html", null);


        }
    }




}