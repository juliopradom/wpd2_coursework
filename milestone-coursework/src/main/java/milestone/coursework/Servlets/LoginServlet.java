package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.User;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Database.IUserLogin;
import milestone.coursework.Database.PasswordHash;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import lombok.NonNull;

public class LoginServlet extends BaseServlet implements IUserLogin {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    private final H2Database h2Database;
    public LoginServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        showView(response, "src/main/resources/webapp/LogIn.html", null);

    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        if(!login(request.getParameter("email"), request.getParameter("password"))) {

            response.sendRedirect("/milestone/login");
        }
        else{

            HttpSession session = request.getSession(true);
            session.setAttribute("user", request.getParameter("email"));

            String sessionid = session.getId();
            response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");

            response.sendRedirect(response.encodeRedirectURL("/milestone/menu"));

        }
    }





    @Override
    public synchronized boolean login(@NonNull String userName,
                                      @NonNull String password) {

        if((userName.equals("user1@user") || userName.equals("user2@user"))){
            return true;  //Default users
        }

        try {
            boolean exists = h2Database.findUser(userName);
            String storedHash = h2Database.findHash(userName);
            return exists && storedHash != null && PasswordHash.validatePassword(password, storedHash);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOG.error("Can't validate password: " + e.getMessage());
            return false;
        }
    }

    @Override
    public boolean register(String userName, String password) {
        return false;
    }


}
