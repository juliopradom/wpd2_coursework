package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MilestoneServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(MenuServlet.class);

    private final H2Database h2Database;

    public MilestoneServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {


            Milestone milestone = h2Database.findMilestone((int) request.getSession().getAttribute("MilestoneID"));


            showView(response, "src/main/resources/webapp/MilestoneView.html", milestone);


        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    }


}

