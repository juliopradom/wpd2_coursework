package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditMilestoneServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    private final H2Database h2Database;

    public EditMilestoneServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {


            Milestone milestone = h2Database.findMilestone((int) request.getSession().getAttribute("MilestoneID"));


            showView(response, "src/main/resources/webapp/EditMilestoneView.html", milestone);


        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Milestone milestone = h2Database.findMilestone((int) request.getSession().getAttribute("MilestoneID"));

        String date = request.getParameter("terminationDate");
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date parseDate = new Date();
        if(!date.equals("")) {
            try {
                parseDate = formatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else parseDate = milestone.getTerminationDate();

        java.sql.Date sqlTerminationDate = new java.sql.Date(parseDate.getTime());

        String name = request.getParameter("name");
        if(name.equals("")) name = milestone.getName();

        String description = request.getParameter("description");
        if(description.equals("")) description = milestone.getDescription();

        Boolean completed = request.getParameter("inputCompleted").equals("yes");


        HttpSession session = request.getSession(false);
        int id = (int) session.getAttribute("MilestoneID");


        int priority = Integer.parseInt(request.getParameter("priority"));


        h2Database.updateMilestone(id, name, description, sqlTerminationDate, completed, priority  );

        String sessionid = session.getId();
        response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");
        response.sendRedirect(response.encodeRedirectURL("/milestone/board-milestone"));

    }


}
