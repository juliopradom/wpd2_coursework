package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EditDomainServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    private final H2Database h2Database;

    public EditDomainServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {

            MilestoneProjects milestoneProjects = h2Database.findMilestoneProjects((String)request.getSession().getAttribute("user"));
            showView(response, "src/main/resources/webapp/EditDomainView.html", milestoneProjects);


        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        MilestoneProjects milestoneProjects = h2Database.findMilestoneProjects((String)request.getSession().getAttribute("user"));

        String domain = request.getParameter("domain");
        if(domain.equals("")) domain = milestoneProjects.getDomain();

        HttpSession session = request.getSession(false);
        int id = (int) session.getAttribute("MilestoneProjectsID");

        h2Database.updateDomain(id, domain);

        String sessionid = session.getId();
        response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");

        response.sendRedirect(response.encodeRedirectURL("/milestone/menu"));

    }


}
