package milestone.coursework.BaseClasses;

import milestone.coursework.BaseClasses.Milestone;

import java.util.ArrayList;
import java.util.List;

public class MilestoneBoard {

    private String name;
    private int id;
    private Boolean shareable;
    private List<Milestone> milestones;
    private String link;

    public MilestoneBoard() {
        this.milestones = new ArrayList<>();
    }

    public MilestoneBoard(String nam){
        this.milestones = new ArrayList<>();
        setName(nam);
    }

    public MilestoneBoard(String nam, List<Milestone> t){
        setMilestones(t);
        setName(nam);
    }


    public void setMilestones(List<Milestone> t) {
        this.milestones = t;
    }
    public void setName(String n) {
        this.name = n;
    }
    public void setID(int id){
        this.id = id;
        this.link ="localhost:9000/milestone/board/" + this.id;
    }
    public void setShareable(boolean shareable){ this.shareable=shareable; }
    public void setLink(String link){ this.link=link; }


    public String getName() {
        return this.name;
    }
    public List<Milestone> getMilestones() {
        return this.milestones;
    }

    public int getID(){ return this.id; }
    public boolean isShareable(){ return this.shareable; }
    public String getLink(){
        if(isShareable())
            return this.link;
        else
            return null;
    }



    public void addMilestone(Milestone newMilestone){
        milestones.add(newMilestone);
    }

    public void deleteMilestone(Integer position){
        milestones.remove(position);
    }

    public void updateMilestone(Integer position, Milestone mil){
        milestones.set(position, mil);
    }



    public void sortMilestones(){

        List<Milestone> complete = new ArrayList<>();
        List<Milestone> notComplete = new ArrayList<>();

        for(int i = 0; i < milestones.size(); ++i){
            if(milestones.get(i).getCompleted())
                complete.add(milestones.get(i));
            else
                notComplete.add(milestones.get(i));
        }

        milestones.clear();
        milestones.addAll(notComplete);
        milestones.addAll(complete);
    }

    public String toString() {
        return ("name: " + name + ", milestones: " + milestones.toString());
    }

}
