package milestone.coursework.BaseClasses;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Milestone {
//variables needed for milestone functionality
    private int id;

    private String name, description;
    private Date startDate, terminationDate, completionDate;
    private boolean completed;   //deadline and actual date
    private int priority;
    private String author;

    public Milestone(){
        author = null;
    }

    public Milestone(String nam, String descript, String auth, Date start, Date termination, boolean completed, int prior)
    {

        setName(nam);
        setDescription(descript);
        setAuthor(auth);
        setStartDate(start);
        setTerminationDate(termination);
        setCompleted(completed);
        setPriority(prior);

    }

    public void setID(int id){ this.id = id; }
   public void setName(String nam){name = nam;}
    public void setDescription(String descript){description = descript;}
    public void setTerminationDate(Date termination){terminationDate=termination;}
    public void setCompletionDate(Date completion){completionDate=completion;}
    public void setCompleted( boolean completion){completed=completion;}
    public void setPriority(Integer prior){priority=prior;}
    public void setStartDate(Date start){startDate=start;}
    public void setAuthor(String auth){author=auth;}



    public Integer getID(){ return id; }
    public String getName(){return name;}
    public String getDescription(){return description;}
    public String getAuthor(){return author;}
    public Date getTerminationDate(){return terminationDate;}
    public Date getStartDate(){return startDate;}
    public Date getCompletionDate(){return completionDate;}
    public boolean getCompleted(){return completed;}
    public Integer getPriority(){return priority;}



    public String toString() {

        String pattern = "MM/dd/yyyy HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern);

        String completion = null;

        if(completed) completion = "Completed \n";

        return ("Name: " + name + "\n   Description: " + description + "\n  Terminate by " + df.format(terminationDate) + "\n " + completion);
    }




}
