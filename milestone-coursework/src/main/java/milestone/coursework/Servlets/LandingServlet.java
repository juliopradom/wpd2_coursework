package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.User;
import milestone.coursework.Mustache.MustacheRender;
import milestone.coursework.Servlets.BaseServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

public class LandingServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    public LandingServlet() {
        super();
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        showView(response, "src/main/resources/webapp/index.html", null);

    }


}

