package milestone.coursework.BaseClasses;

public class Project {
    private String projectName;
    private String projectDes;
    private String startDate;
    private String endDate;



    public Project(String projectName, String projectDes, String startDate, String endDate){

    }
    public Project(){
        this.projectName = null;
        this.projectDes = null;
        this.startDate = null;
        this.endDate=null;
    }
    public String getProjectName(){return projectName;}
    public String getProjectDesc(){return projectDes;}
    public String getStartDate(){return startDate;}
    public String getEndDate(){return endDate;}


    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    public void setProjectDes(String projectDes) {
        this.projectDes = projectDes;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
}
