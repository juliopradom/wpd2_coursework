
package milestone.coursework.Database;

import milestone.coursework.BaseClasses.Milestone;
import milestone.coursework.BaseClasses.MilestoneBoard;
import milestone.coursework.BaseClasses.MilestoneProjects;
import milestone.coursework.BaseClasses.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;


public class H2Database implements AutoCloseable {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(H2Database.class);

    public static final String MEMORY = "jdbc:h2:mem:milestone";
    public static final String FILE = "jdbc:h2:~/milestone";

    private Connection connection;

    static Connection getConnection(String db) throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");  // ensure the driver class is loaded when the DriverManager looks for an installed class. Idiom.
        return DriverManager.getConnection(db, "sa", "");  // default password, ok for embedded.
    }

    public H2Database() {
        this(FILE);
    }

    public H2Database(String db) {
        try {
            connection = getConnection(db);
            loadResource("/Milestone.sql");
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }

        if(!findUser("user1@user")) {

            addUser(new User("user1@user", ""));
            addUser(new User("user2@user", ""));

            MilestoneProjects milestoneProjects1 = findMilestoneProjects("user1@user");
            MilestoneProjects milestoneProjects2 = findMilestoneProjects("user2@user");

            int mb11 = addMilestoneBoard(milestoneProjects1.getID(), "WDP2_Coursework", true);
            int mb12 = addMilestoneBoard(milestoneProjects1.getID(), "My tasks", false);

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(2019, 5, 1, 17, 0);
            java.util.Date date = cal.getTime();
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());


            Calendar cal2 = Calendar.getInstance();
            java.util.Date date2 = cal2.getTime();
            java.sql.Date sqlDate2 = new java.sql.Date(date2.getTime());


            addMilestone(mb11, "Introduction", "Introduction of the coursework", "user1", sqlDate, sqlDate2, false, 5);

            cal.set(2019, 5, 2, 9, 30);
            date = cal.getTime();
            sqlDate = new java.sql.Date(date.getTime());
            int idd = addMilestone(mb11, "Code", "Code section of the coursework", "user1", sqlDate, sqlDate2, true, 7);
            updateMilestone(idd, "Code", "Code section of the coursework", sqlDate, true, 7);


            cal.set(2019, 5, 15, 14, 0);
            date = cal.getTime();
            sqlDate = new java.sql.Date(date.getTime());
            addMilestone(mb11, "Report", "Report of the coursework", "user1", sqlDate, sqlDate2, false, 7);

            cal.set(2019, 6, 15, 14, 0);
            date = cal.getTime();
            sqlDate = new java.sql.Date(date.getTime());
            addMilestone(mb12, "Documentation", "Hand in documentation for next year", "user1", sqlDate, sqlDate2, false, 10);

            cal.set(2019, 6, 1, 9, 0);
            date = cal.getTime();
            sqlDate = new java.sql.Date(date.getTime());
            addMilestone(mb12, "Group Meeting", "CGroup meeting for external project", "user1", sqlDate, sqlDate2, false, 6);

        }

    }

    /** Only class use**/

    private int getUserID(String name){
        final String USER_QUERY = "SELECT UserID FROM User WHERE Name=?";
        int id = -1;
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setString(1,name);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                id = rs.getInt(1);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return id;
    }

   /** Add a new user to the database **/
    public boolean addUser(User person) {
        if (!findUser(person.getName())) {
            final String ADD_PERSON_QUERY = "INSERT INTO User (Name, Password) VALUES (?,?)";
            try (PreparedStatement ps = connection.prepareStatement(ADD_PERSON_QUERY)) {
                ps.setString(1, person.getName());
                ps.setString(2, person.getPassword());
                ps.execute();
                addEmptyMilestoneProjects(person.getName());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            return true;
        }
        else return false;
    }

    /** Check if a user already exists on the database **/
    public boolean findUser(String name){
        final String USER_QUERY = "SELECT UserID, Name FROM User WHERE Name=?";
        User user = new User();
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setString(1,name);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                user.setId(rs.getInt(1));
                user.setName(rs.getString(2));

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return user.getName() != null && user.getId() != null ;
    }

    /** hash functiion **/
    public String findHash(String name){
        final String USER_QUERY = "SELECT Password FROM User WHERE Name=?";
        String hash = null;
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setString(1,name);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                hash = rs.getString(1);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return hash ;
    }

    /** retrieve front-end MilestonePfojects for a user **/
    public MilestoneProjects findMilestoneProjects(String name){
        int userID = getUserID(name);
        final String USER_QUERY = "SELECT MilestoneProjectsID, Domain FROM MilestoneProjects WHERE UserID=?";
        MilestoneProjects milestoneProjects = new MilestoneProjects();
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setInt(1,userID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                milestoneProjects.setID( rs.getInt(1));
                milestoneProjects.setDomain(rs.getString(2));
            }
            else{
                milestoneProjects.setID( -1 );
                milestoneProjects.setDomain(null);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        final String USER_QUERY_2 = "SELECT mb.MilestoneBoardID, mb.Name, mb.Shareable " +
                "FROM MilestoneProjects mp " +
                "INNER JOIN ProjectsBoardLink pbl ON mp.MilestoneProjectsID = pbl.MilestoneProjectsID " +
                "INNER JOIN MilestoneBoard mb ON mb.MilestoneBoardID = pbl.MilestoneBoardID " +
                "WHERE mp.MilestoneProjectsID=?";
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY_2)) {
            ps.setInt(1,milestoneProjects.getID());
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                MilestoneBoard newMilestoneBoard = new MilestoneBoard();
                newMilestoneBoard.setID(rs.getInt(1));
                newMilestoneBoard.setName(rs.getString(2));
                newMilestoneBoard.setShareable(rs.getString(3).equals("Y"));
                milestoneProjects.addMilestoneBoard(newMilestoneBoard);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return milestoneProjects ;
    }

    /** add an empty MilestoneProjects for a user (called first time a user is registered). Returns MilestoneProjectID for created MIlestoneProject**/

    public int addEmptyMilestoneProjects(String name){
        int userID = getUserID(name);
        final String USER_QUERY= "INSERT INTO MilestoneProjects (UserID, Domain) VALUES (?,?)";
        int out = 0;

        if(findMilestoneProjects(name).getDomain() == null) {

            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY, new String[]{"MilestoneProjectsID"})) {
                ps.setInt(1, userID);
                ps.setString(2, name + "_domain");
                ps.execute();
                ResultSet rs = ps.getGeneratedKeys();
                if(rs.next())
                   out = rs.getInt(1);

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }


        }

        return out;


    }

    /** updates the domain **/

    public void updateDomain(int MilestoneProjectsID, String newDomain){

        final String USER_QUERY= "UPDATE MilestoneProjects SET Domain=? WHERE MilestoneProjectsID=? ";

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setString(1, newDomain);
            ps.setInt(2, MilestoneProjectsID);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    /** retrieve front-end MilestoneBoard **/
    public MilestoneBoard findMilestoneBoard(int milestoneBoardID){

        MilestoneBoard outMilestoneBoard = new MilestoneBoard();

        final String USER_QUERY = "SELECT MilestoneBoardID, Name, Shareable FROM MilestoneBoard WHERE MilestoneBoardID=?";
        MilestoneProjects milestoneProjects = new MilestoneProjects();
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setInt(1, milestoneBoardID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                outMilestoneBoard.setID( rs.getInt(1));
                outMilestoneBoard.setName(rs.getString(2));
                outMilestoneBoard.setShareable(rs.getString(3).equals("Y"));
            }
            else{
                outMilestoneBoard.setID( -1 );
                outMilestoneBoard.setName(null);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        final String USER_QUERY_2 = "SELECT m.MilestoneID, m.Name, m.Priority, m.TerminationDate, m.Completed " +
                "FROM MilestoneBoard mb " +
                "INNER JOIN Milestone m ON mb.MilestoneBoardID = m.MilestoneBoardID " +
                "WHERE mb.MilestoneBoardID=?";
        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY_2)) {
            ps.setInt(1,milestoneBoardID);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                Milestone newMilestone = new Milestone();
                newMilestone.setID(rs.getInt(1));
                newMilestone.setName(rs.getString(2));
                newMilestone.setPriority(rs.getInt(3));
                newMilestone.setTerminationDate(rs.getDate(4));
                newMilestone.setCompleted(rs.getString(5).equals("Y"));
                outMilestoneBoard.addMilestone(newMilestone);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return outMilestoneBoard ;

    }


    /** adding an empty new MilestoneBoard **/
    public int addMilestoneBoard(int MilestoneProjectsID, String name, Boolean shareable){

        final String USER_QUERY= "INSERT INTO MilestoneBoard (Name, Shareable) VALUES (?,?)";
        final String USER_QUERY_2= "INSERT INTO ProjectsBoardLink (MilestoneProjectsID, MilestoneBoardID) VALUES (?,?)";
        int out = 0;

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY, new String[]{"MilestoneBoardID"})) {
            ps.setString(1, name);
            ps.setString(2, shareable ? "Y" : "N");
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next())
               out = rs.getInt(1);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if(out != 0){

            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY_2)) {
                ps.setInt(1, MilestoneProjectsID);
                ps.setInt(2, out);
                ps.execute();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }


        return out;




    }

    /** update a specific milestoneBoard **/
    public void updateMilestoneBoard(int MilestoneBoardID, String name, Boolean shareable){

        final String USER_QUERY= "UPDATE MilestoneBoard SET Name=?, Shareable=? WHERE MilestoneBoardID=? ";

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setString(1, name);
            ps.setString(2, shareable ? "Y" : "N");
            ps.setInt(3, MilestoneBoardID);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    /** linking an existing milestoneBoard with a user's projects **/

    public void shareMilestoneBoard(int MilestoneProjectsID, int MilestoneBoardID){


        final String USER_QUERY= "INSERT INTO ProjectsBoardLink (MilestoneProjectsID, MilestoneBoardID) VALUES (?,?)";
        final String USER_QUERY_2 = "SELECT MilestoneBoardID FROM ProjectsBoardLink WHERE MilestoneProjectsID=? AND MilestoneBoardID=?";
        boolean exists = false;

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY_2)) {
            ps.setInt(1, MilestoneProjectsID );
            ps.setInt(2, MilestoneBoardID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                exists = true;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if(!exists) {
            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
                ps.setInt(1, MilestoneProjectsID);
                ps.setInt(2, MilestoneBoardID);
                ps.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }



    }


    /** Delete a link with a user or the complete milestoneBoard in case it is the last copy **/

    public void deleteMilestoneBoard(int MilestoneProjectsID, int MilestoneBoardID){


        final String USER_QUERY= "DELETE FROM ProjectsBoardLink WHERE MilestoneBoardID=? AND MilestoneProjectsID=?";
        final String USER_QUERY_2= "SELECT MilestoneBoardID FROM ProjectsBoardLink WHERE MilestoneBoardID=?";
        final String USER_QUERY_3= "DELETE FROM MilestoneBoard WHERE MilestoneBoardID=?";

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setInt(1, MilestoneBoardID );
            ps.setInt(2, MilestoneProjectsID );
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY_2)) {
            ps.setInt(1, MilestoneBoardID );
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                try (PreparedStatement ps2 = connection.prepareStatement(USER_QUERY_3)) {
                    ps.setInt(1, MilestoneBoardID );
                    ps.execute();
                    deleteMilestone(MilestoneBoardID, 0);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }


    /** Add a new Milestone **/
    public int addMilestone(int MilestoneBoardID, String name, String description, String author, Date termination, Date start, boolean complete, int priority){

        final String USER_QUERY= "INSERT INTO Milestone (MilestoneBoardID, Name, Description, Author, TerminationDate, StartDate, Completed, Priority) VALUES (?,?,?,?,?,?,?,?)";
        int out = 0;

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY, new String[]{"MilestoneID"})) {
            ps.setInt(1, MilestoneBoardID);
            ps.setString(2, name);
            ps.setString(3, description);
            ps.setString(4, author);
            ps.setDate(5, termination);
            ps.setDate(6, start);
            ps.setString(7, complete ? "Y" : "N");
            ps.setInt(8, priority);
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next())
                out = rs.getInt(1);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return out;

    }


    /** Update a milestone **/
    public void updateMilestone(int MilestoneID, String name, String description, Date termination, boolean complete, int priority){


        if(!complete) {

            final String USER_QUERY = "UPDATE Milestone SET Name=?, Description=?, TerminationDate=?, Completed=?, Priority=? WHERE MilestoneID=? ";

            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
                ps.setString(1, name);
                ps.setString(2, description);
                ps.setDate(3, termination);
                ps.setString(4,"N");
                ps.setInt(5, priority);
                ps.setInt(6, MilestoneID);
                ps.execute();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        else{
            final String USER_QUERY = "UPDATE Milestone SET Name=?, Description=?, TerminationDate=?, Completed=?, CompletionDate=?, Priority=? WHERE MilestoneID=? ";

            Calendar cal = Calendar.getInstance();
            java.util.Date date = cal.getTime();
            java.sql.Date completionDate = new java.sql.Date(date.getTime());

            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
                ps.setString(1, name);
                ps.setString(2, description);
                ps.setDate(3, termination);
                ps.setString(4, "Y");
                ps.setDate(5, completionDate);
                ps.setInt(6, priority);
                ps.setInt(7, MilestoneID);
                ps.execute();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }





    /** retrieve a specific milestone **/

    public Milestone findMilestone(int MilestoneID){

        Milestone outMilestone = new Milestone();

        final String USER_QUERY = "SELECT MilestoneID, Name, Description, Author, TerminationDate, StartDate, Completed, CompletionDate, Priority FROM Milestone WHERE MilestoneID=?";

        try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
            ps.setInt(1, MilestoneID);

            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                outMilestone.setID( rs.getInt(1));
                outMilestone.setName(rs.getString(2));
                outMilestone.setDescription(rs.getString(3));
                outMilestone.setAuthor(rs.getString(4));
                outMilestone.setTerminationDate(rs.getDate(5));
                outMilestone.setStartDate(rs.getDate(6));
                outMilestone.setCompleted(rs.getString(7).equals("Y"));
                outMilestone.setCompletionDate(rs.getDate(8));
                outMilestone.setPriority(rs.getInt(9));
            }
            else{
                outMilestone.setID( -1 );
                outMilestone.setName(null);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return outMilestone ;
    }



    /** Delete a specific milestone. If MilestoneID is equal to 0, all the milestone for the MilestoneBoard are deleted **/

    public void deleteMilestone(int MilestoneBoardID, int MilestoneID){

        final String USER_QUERY= "DELETE FROM Milestone WHERE MilestoneBoardID=? AND MilestoneID=?";
        final String USER_QUERY_2= "DELETE FROM Milestone WHERE MilestoneBoardID=?";

        if(MilestoneID == 0){

            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY_2)) {
                ps.setInt(1, MilestoneBoardID );
                ps.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        }

        else{
            try (PreparedStatement ps = connection.prepareStatement(USER_QUERY)) {
                ps.setInt(1, MilestoneBoardID );
                ps.setInt(2, MilestoneID );
                ps.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }















    @Override
    public void close() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    private void loadResource(String name) {
        try {
            String cmd = new Scanner(getClass().getResource(name).openStream()).useDelimiter("\\Z").next();
            PreparedStatement ps = connection.prepareStatement(cmd);
            ps.execute();
        } catch (SQLException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
