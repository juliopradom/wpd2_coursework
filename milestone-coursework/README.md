# Milestone Application

Milestone Final Application

## Requirements

This is a Git repo. 
To run it you need [Java 11][2], [Maven][3], [Mustache][4] and (optionally) [Intellij][5]. 

# Get started

Clone the repo. 
Import the project as a Maven project into IntelliJ.
Build the project. Then run it. 

The first time this may take a while as a lot of dependencies need to be installed.  Future runs will be quicker.

There will be a lot of console output finishing with:

    INFO: App Server is now running

By default you are running from the address `http://localhost:9000` on the development server. All the paths below are relative to this, which may differ for you if you change it.


    
## Technology stack in the starter

The starter uses [Jetty][1] to run an embedded web server.

The main class is Runner.java. This starts an embedded Jetty Server. 

The single servlet at this stage is LandingServlet.java.

[1]: https://eclipse.org/jetty/
[2]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
[3]: http://maven.apache.org/download.cgi
[4]: http://www.jetbrains.com/idea/
[5]: https://github.com/google/guice/wiki/GettingStarted
[6]: https://github.com/google/guice/wiki/Servlets
