
package milestone.coursework;

import milestone.coursework.Database.H2Database;
import milestone.coursework.Servlets.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

public class Runner {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);

    private static final int PORT = 9000;
    private final String name;
    private final H2Database h2Database;

    private Runner(String name) {
        this.name = name;
        this.h2Database = new H2Database();
    }

    //the start method, starts the server and sets up the servlets that the server knows
    private void start() throws Exception {

        //creating an instance of the server listening on the specified port
        Server server = new Server(PORT);

        //creating a servlet context handler defining the environment for the servlets to run, e.g. which folder
        //the root is mapped to

        /** The handler just contain every servlet we define for our app **/
        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "src/main/resources/webapp/index.html");

        //APP SERVLETS

        handler.addServlet(new ServletHolder(new LandingServlet()), "/"); /** Landing Page (login and registration will be in "localhost:9000/"  **/
        handler.addServlet(new ServletHolder(new RegistrationServlet(h2Database)), "/milestone/registration"); /** registration in "localhost:9000/registration"  **/
        handler.addServlet(new ServletHolder(new LoginServlet(h2Database)), "/milestone/login"); /** etc, ... **/
        handler.addServlet(new ServletHolder(new MenuServlet(h2Database)), "/milestone/menu");
        handler.addServlet(new ServletHolder(new AddProjectServlet(h2Database)), "/milestone/menu/AddProject");
        handler.addServlet(new ServletHolder(new EditDomainServlet(h2Database)), "/milestone/menu/EditDomain");
        handler.addServlet(new ServletHolder(new AboutAuthorsServlet()), "/milestone/menu/AboutAuthors");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/milestone/menu/Logout");

        handler.addServlet(new ServletHolder(new MilestoneBoardServlet(h2Database)), "/milestone/board/*");

        handler.addServlet(new ServletHolder(new EditProjectServlet(h2Database)), "/milestone/menu/EditProject");
        handler.addServlet(new ServletHolder(new AddMilestoneServlet(h2Database)), "/milestone/menu/AddMilestone");

        handler.addServlet(new ServletHolder(new MilestoneServlet(h2Database)), "/milestone/board-milestone");
        handler.addServlet(new ServletHolder(new EditMilestoneServlet(h2Database)), "/milestone/board-milestone/EditMilestone");
        /** Although it was set in labs, we don't really need the default servlet

        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");
         **/


        //start the server
        server.start();
        LOG.info("Server started, will run until terminated");
        server.join();

    }

    public static void main(String[] args) {
        try {
            LOG.info("starting");
            Locale.setDefault(Locale.UK);
            new Runner("Milestone").start();
        } catch (Exception e) {
            LOG.error("Unexpected error running milestone: " + e.getMessage());
        }
    }


}
