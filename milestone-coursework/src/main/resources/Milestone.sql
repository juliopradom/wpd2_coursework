CREATE TABLE IF NOT EXISTS User (
  UserID int AUTO_INCREMENT PRIMARY KEY,
  Name VARCHAR(255) NOT NULL ,
  Password VARCHAR(255),
  UNIQUE(Name)
);

CREATE TABLE IF NOT EXISTS MilestoneProjects (
  MilestoneProjectsID int AUTO_INCREMENT,
  UserID int,
  Domain VARCHAR(255),
  PRIMARY KEY(MilestoneProjectsID, UserID),
  CONSTRAINT FK_UserID1 FOREIGN KEY (UserID)
    REFERENCES User(UserID)
);


CREATE TABLE IF NOT EXISTS MilestoneBoard (
  MilestoneBoardID int AUTO_INCREMENT PRIMARY KEY,
  Name VARCHAR(255),
  Shareable CHAR(1)

);


CREATE TABLE IF NOT EXISTS ProjectsBoardLink (
  MilestoneProjectsID int,
  MilestoneBoardID int,
  PRIMARY KEY (MilestoneProjectsID, MilestoneBoardID),
  CONSTRAINT FK_MilestoneProjectsID FOREIGN KEY (MilestoneProjectsID)
    REFERENCES MilestoneProjects(MilestoneProjectsID),
  CONSTRAINT FK_MilestoneBoardID1 FOREIGN KEY (MilestoneBoardID)
    REFERENCES MilestoneBoard(MilestoneBoardID),
);


CREATE TABLE IF NOT EXISTS Milestone (
  MilestoneID int AUTO_INCREMENT,
  MilestoneBoardID int,
  Name VARCHAR(255),
  Description VARCHAR(255),
  Author VARCHAR(255),
  TerminationDate DATETIME,
  CompletionDate DATETIME,
  Completed CHAR(1),
  StartDate DATETIME,
  Priority int,
  PRIMARY KEY (MilestoneID, MilestoneBoardID),
  CONSTRAINT FK_MilestoneBoardID2 FOREIGN KEY (MilestoneBoardID)
    REFERENCES MilestoneBoard(MilestoneBoardID)

);










