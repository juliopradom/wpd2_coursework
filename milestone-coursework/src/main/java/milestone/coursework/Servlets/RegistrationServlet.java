package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.User;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Database.IUserLogin;
import milestone.coursework.Database.PasswordHash;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import lombok.NonNull;

public class RegistrationServlet extends BaseServlet implements IUserLogin {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    private final H2Database h2Database;
    public RegistrationServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        showView(response, "src/main/resources/webapp/SignUp.html", null);

    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(register(request.getParameter("email"), request.getParameter("password"))) {
            response.sendRedirect("/milestone");
        }
        else{
            response.sendRedirect("/milestone/registration");
        }
    }

    @Override
    public boolean login(String userName, String password) {
        return false;
    }

    @Override
    public synchronized boolean register(@NonNull String userName,
                                         @NonNull String password) {

        try {
            String hash = PasswordHash.createHash(password);
            User user = new User(userName, hash);
            return h2Database.addUser(user);

        } catch (Exception e) {
            LOG.error("Can't hash password <" + password + ">: "
                    + e.getMessage());
            return false;
        }
    }
}
