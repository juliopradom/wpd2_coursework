package milestone.coursework.BaseClasses;

import java.util.ArrayList;
import java.util.List;

public class MilestoneProjects {

    private int id;
    private String domain;

    private List<MilestoneBoard> projectList;

    public MilestoneProjects() {
        this.projectList = new ArrayList<>();
    }

    public MilestoneProjects(String dom){
        this.projectList = new ArrayList<>();
        setDomain(dom);
    }

    public MilestoneProjects(String dom, List<MilestoneBoard> t){
        setProjectList(t);
        setDomain(dom);
    }


    public void setProjectList(List<MilestoneBoard> t) {
        this.projectList = t;
    }
    public void setDomain(String dom) { this.domain = dom; }
    public void setID(int id){ this.id = id; }


    public String getDomain() {
        return this.domain;
    }
    public List<MilestoneBoard> getMilestoneBoards() {
        return this.projectList;
    }
    public int getID(){ return this.id; }



    public void addMilestoneBoard(MilestoneBoard newMilestoneBoard){
        projectList.add(newMilestoneBoard);
    }

    public void deleteMilestoneBoard(Integer position){
        projectList.remove(position);
    }



    public String toString() {
        return ("Domain: " + domain + "ID" + id + "\n Projects: " + projectList.toString());
    }
}
