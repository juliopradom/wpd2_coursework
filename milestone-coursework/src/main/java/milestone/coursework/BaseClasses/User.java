
package milestone.coursework.BaseClasses;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class User {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(User.class);
    private Integer id;
    private String name;
    private String password;



    public User(String name, String password) {
        this.id = null;
        this.name = name;
        this.password = password;

    }

    public User() {
        this.name = null;
        this.password = null;
        this.id = null;

    }

    public String getName(){return name;}
    public String getPassword(){return password;}
    public Integer getId(){return id;}

    public void setName(String name){
        this.name = name;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setId(Integer id){
        this.id = id;
    }



}
