package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddMilestoneServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    private final H2Database h2Database;

    public AddMilestoneServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {


            showView(response, "src/main/resources/webapp/AddMilestoneView.html", null);


        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String date = request.getParameter("terminationDate");
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date parseDate = new Date();

        if(!date.equals("")) {
            try {
                parseDate = formatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else {

            try {
                parseDate = formatter.parse("12/31/2099");
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        java.sql.Date sqlTerminationDate = new java.sql.Date(parseDate.getTime());

        String name = request.getParameter("name");
        String description = request.getParameter("description");





        HttpSession session = request.getSession(false);
        int id = (int) session.getAttribute("MilestoneBoardID");
        String author = (String) session.getAttribute("user");

        Calendar cal2 = Calendar.getInstance();
        java.util.Date date2 = cal2.getTime();
        java.sql.Date sqlDate2 = new java.sql.Date(date2.getTime());

        int priority = Integer.parseInt(request.getParameter("priority"));


        h2Database.addMilestone(id, name, description, author, sqlTerminationDate, sqlDate2, false, priority  );

        String sessionid = session.getId();
        response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");

        response.sendRedirect(response.encodeRedirectURL("/milestone/board/"));


    }


}