package milestone.coursework.Servlets;

import milestone.coursework.BaseClasses.*;
import milestone.coursework.Database.H2Database;
import milestone.coursework.Mustache.MustacheRender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EditProjectServlet extends BaseServlet {

    static final Logger LOG = LoggerFactory.getLogger(RegistrationServlet.class);

    private final H2Database h2Database;

    public EditProjectServlet(H2Database h2Person) {
        super();
        this.h2Database = h2Person;
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(authOK(request,response)) {

            MilestoneBoard milestoneBoard = h2Database.findMilestoneBoard((int)request.getSession().getAttribute("MilestoneBoardID"));
            showView(response, "src/main/resources/webapp/EditProjectView.html", milestoneBoard);


        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        MilestoneBoard milestoneBoard = h2Database.findMilestoneBoard((int)request.getSession().getAttribute("MilestoneBoardID"));

        String name = request.getParameter("name");
        if(name.equals("")) name = milestoneBoard.getName();

        boolean share = request.getParameter("inputShareable").equals("yes");

        HttpSession session = request.getSession(false);

        h2Database.updateMilestoneBoard((int)session.getAttribute("MilestoneBoardID"), name, share);

        String sessionid = session.getId();
        response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");

        response.sendRedirect(response.encodeRedirectURL("/milestone/board/"));

    }


}
